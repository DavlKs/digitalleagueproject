package com.davletova.project.service;

import com.davletova.project.comparator.HumanAgeComparator;
import com.davletova.project.model.Human;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class HumanServiceImpl implements HumanService {

    @Override
    public List<Human> getListOfSuitablePeople(List<Human> list) {
        return list.stream()
                .filter(human -> human.getAge() <= 20 && human.getSurname().matches("[а-дА-Д].*"))
                .collect(Collectors.toList());
    }

    @Override
    public boolean checkIfThereIsMatch(Human human, List<Human> list) {
        return list.contains(human);
    }

    @Override
    public Map<Human, Integer> findRepetitions(List<Human> list) {
        Map<Human, Integer> result = new TreeMap<>(new HumanAgeComparator());
        for (Human h: list) {
            if (result.containsKey(h)) {
                result.replace(h, result.get(h) + 1);
            } else {
                result.put(h, 1);
            }
        }
        return result;
    }
}
