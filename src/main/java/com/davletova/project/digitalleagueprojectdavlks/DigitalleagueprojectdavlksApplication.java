package com.davletova.project.digitalleagueprojectdavlks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigitalleagueprojectdavlksApplication {

    public static void main(String[] args) {
        SpringApplication.run(DigitalleagueprojectdavlksApplication.class, args);
    }

}
