package com.davletova.project.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;

@Repository
public class CurrencyDaoImpl implements CurrencyDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Double getCurrencyRate(String to, String from) {
        Query query = entityManager.createQuery("select " + to + " from Currency where name = :from");
        query.setParameter("from", from);
        return (Double) query.getSingleResult();
    }
}
