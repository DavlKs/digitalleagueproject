package com.davletova.project.service;

import com.davletova.project.model.Human;

import java.util.List;
import java.util.Map;

public interface HumanService {

        List<Human> getListOfSuitablePeople(List<Human> list);

        boolean checkIfThereIsMatch(Human human, List<Human> list);

        Map<Human, Integer> findRepetitions(List<Human> list);



}
