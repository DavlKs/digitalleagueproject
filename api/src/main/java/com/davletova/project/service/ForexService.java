package com.davletova.project.service;

public interface ForexService {

    Double getCurrencyRate(String to, String from);
}
