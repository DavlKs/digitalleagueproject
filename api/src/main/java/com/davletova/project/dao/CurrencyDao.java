package com.davletova.project.dao;

public interface CurrencyDao {

    Double getCurrencyRate(String to, String from);
}
