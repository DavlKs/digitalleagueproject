package com.davletova.project.model;

import java.time.LocalDate;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private String patronymicName;
    private int age;
    private String sex;
    private LocalDate dateOfBirth;

    public Human() {
    }

    public Human(String name, String surname, String patronymicName, int age, String sex, LocalDate dateOfBirth) {
        this.name = name;
        this.surname = surname;
        this.patronymicName = patronymicName;
        this.age = age;
        this.sex = sex;
        this.dateOfBirth = dateOfBirth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymicName() {
        return patronymicName;
    }

    public void setPatronymicName(String patronymicName) {
        this.patronymicName = patronymicName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age
                && name.equals(human.name)
                && surname.equals(human.surname)
                && patronymicName.equals(human.patronymicName)
                && sex.equals(human.sex)
                && dateOfBirth.equals(human.dateOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, patronymicName, age, sex, dateOfBirth);
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'';
    }
}
