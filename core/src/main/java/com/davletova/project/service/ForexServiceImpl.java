package com.davletova.project.service;

import com.davletova.project.dao.CurrencyDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ForexServiceImpl implements ForexService {

    private CurrencyDao currencyDao;

    @Autowired
    public ForexServiceImpl(CurrencyDao currencyDao) {
        this.currencyDao = currencyDao;
    }

    @Override
    public Double getCurrencyRate(String to, String from) {
        return currencyDao.getCurrencyRate(to, from);
    }
}
