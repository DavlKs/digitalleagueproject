INSERT INTO locations (street_address, city) VALUES
('Gorkogo, 165', 'Nizhniy Novgorod'),
('Kutuzovskaya, 83', 'Moskva'),
('Lenina, 18', 'Kazan');

INSERT INTO jobs VALUES
('FDA', 'Front desk agent', 25000, 45000),
('HKM', 'Maid', 20000, 45000),
('En', 'Engineer', 35000, 65000),
('GM', 'General Manager', 150000, 350000),
('SA', 'Sales agent', 35000, 60000),
('RA', 'Reservation agent', 30000, 52000),
('RW', 'Waiter', 15000, 34000),
('RC', 'Chief', 50000, 97000),
('HKSM', 'Senior maid', 35000, 72000),
('FDM', 'Front desk manager', 64000, 84000),
('HEn', 'Head Engineer', 57000, 83000);

INSERT INTO departments (department_id, department_name, location_id) VALUES
(1, 'Front desk', 1),
(2, 'Housekeeping', 1),
(3, 'Engineering', 1),
(4, 'Management', 2),
(5, 'Sales', 3),
(6, 'Reservation', 3),
(7, 'Restaurant', 1);

INSERT INTO employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, department_id)
VALUES
(1, 'Konstantin', 'Klyuev', 'klyuev@mail.ru', '89635261950', '18-04-2019', 'GM', 268000, 4),
(2, 'Kseniya', 'Zemskova', 'zem@mail.ru', '89528501837', '05-02-2016', 'FDM', 73000, 1),
(3, 'Tatyana', 'Sinitsyna', 'sinitsa@mail.ru', '89630782641', '10-08-2018', 'RA', 48000, 6),
(4, 'Olga', 'Zimina', 'zima@mail.ru', '89472074829', '10-09-2017', 'SA', 39000, 5),
(5, 'Yuriy', 'Prisyazhnyuk', 'yura@mail.ru', '89651906385', '01-03-2019', 'FDA', 34000, 1),
(6, 'Igor', 'Trumanov', 'trueman@mail.ru', '89638190472', '23-12-2018', 'HEn', 78000, 3),
(7, 'Oleg', 'Timichev', 'olezha@mail.ru', '89620896894', '08-10-2020', 'En', 42000, 3),
(8, 'Marina', 'Ivanova', 'mara@mail.ru', '89527891207', '29-07-2020', 'HKSM', 68000, 2),
(9, 'Dmitriy', 'Lobanov', 'lob@mail.ru', '89776522908', '10-01-2019', 'RC', 92000, 7),
(10, 'Anastasiya', 'Lyutina', 'lyutik@mail.ru', '89008967432', '01-02-2021', 'RW', 31000, 7),
(11, 'Galina', 'Morozova', 'moroz@mail.ru', '89775399201', '15-11-2017', 'HKM', 31000, 2);

INSERT INTO job_history (employee_id, start_date, job_id, department_id)
VALUES
(1, '20-04-2019', 'GM', 4),
(2, '06-02-2016', 'FDM', 1),
(3, '10-08-2018', 'RA', 6),
(4, '13-09-2017', 'SA', 5),
(5, '01-03-2019', 'FDA', 1),
(6, '24-12-2018', 'HEn', 3),
(7, '10-10-2020', 'En', 3),
(8, '29-07-2020', 'HKSM', 2),
(9, '17-10-2019', 'RC', 7),
(10, '01-02-2021', 'RW', 7),
(11, '15-11-2017', 'HKM', 2);

Update employees SET manager_id = 1 WHERE employee_id = 2
                                       OR employee_id = 3
                                       OR employee_id = 4
                                       OR employee_id = 6
                                       OR employee_id = 8
                                       OR employee_id = 9;

Update employees SET manager_id = 2 WHERE employee_id = 5;
Update employees SET manager_id = 6 WHERE employee_id = 7;
Update employees SET manager_id = 9 WHERE employee_id = 10;
Update employees SET manager_id = 8 WHERE employee_id = 11;

UPDATE departments SET manager_id = 2 WHERE department_name = 'Front desk';
UPDATE departments SET manager_id = 1 WHERE department_name = 'Management';
UPDATE departments SET manager_id = 3 WHERE department_name = 'Reservation';
UPDATE departments SET manager_id = 4 WHERE department_name = 'Sales';
UPDATE departments SET manager_id = 8 WHERE department_name = 'Housekeeping';
UPDATE departments SET manager_id = 9 WHERE department_name = 'Restaurant';
UPDATE departments SET manager_id = 6 WHERE department_name = 'Engineering';
