package com.davletova.project;

import com.davletova.project.dao.CurrencyDao;
import com.davletova.project.service.ForexExchangeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = DemoApplication.class)
class DemoApplicationTests {

    @Autowired
    private CurrencyDao currencyDao;

    @Autowired
    private ForexExchangeService forexExchangeService;

    @Test
    void exchangeMoney() {
        Double correctRate = 102.82;
        Double correctResult = 205.64;

        Double actualRate = currencyDao.getCurrencyRate("RUB", "GBP");
        Assertions.assertEquals(correctRate, actualRate);

        Double actualResult = forexExchangeService.exchangeMoney("RUB", "GBP", 2);
        Assertions.assertEquals(correctResult, actualResult);

    }


}

