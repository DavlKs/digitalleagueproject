package com.davletova.project;


import com.davletova.project.model.Human;
import com.davletova.project.service.HumanServiceImpl;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class HumanServiceTest {


    List<Human> getHumanList() {
        List<Human> list = new ArrayList<>();
        Human h1 = new Human("Михаил", "Канвальдов", "Юрьевич", 23, "male", LocalDate.of(1997, 12,5));
        list.add(h1);
        Human h2 = new Human("Екатерина", "Нильсова", "Александровна", 27, "female", LocalDate.of(1993, 12,10));
        list.add(h2);
        Human h3 = new Human("Клавдия", "Тидеманова", "Игоревна", 32, "male", LocalDate.of(1988, 10,10));
        list.add(h3);
        Human h4 = new Human("Марта", "Дильсова", "Юрьевна", 17, "female", LocalDate.of(2003, 06, 25));
        list.add(h4);
        return list;
    }

    @Test
    void getListOfSuitablePeople() {
        HumanServiceImpl hs = new HumanServiceImpl();

        List<Human> list = getHumanList();
        List<Human> correctList = new ArrayList<>();
        correctList.add(new Human("Марта", "Дильсова", "Юрьевна", 17, "female", LocalDate.of(2003, 06, 25)));

        List<Human> actualList = hs.getListOfSuitablePeople(list);

        assertEquals(correctList.size(), actualList.size());
        assertEquals(correctList.get(0), actualList.get(0));

    }

    @Test
    void checkIfThereIsMatch() {
        boolean result1 = false;
        boolean result2 = false;


        HumanServiceImpl hs = new HumanServiceImpl();
        List<Human> list = getHumanList();

        Human template1 = new Human("Клавдия", "Тидеманова", "Игоревна", 32, "male", LocalDate.of(1988, 10,10));
        Human template2 = new Human("Максим", "Нильсов", "Юрьевич", 17, "female", LocalDate.of(2003, 06, 25));

        result1 = hs.checkIfThereIsMatch(template1, list);
        result2 = hs.checkIfThereIsMatch(template2, list);

        assertTrue(result1);
        assertFalse(result2);
    }

    @Test
    void findRepetitions() {
        HumanServiceImpl hs = new HumanServiceImpl();
        List<Human> list = getHumanList();

        Human h1 = new Human("Михаил", "Канвальдов", "Юрьевич", 23, "male", LocalDate.of(1997, 12,5));
        list.add(h1);
        Human h2 = new Human("Екатерина", "Нильсова", "Александровна", 27, "female", LocalDate.of(1993, 12,10));
        list.add(h2);
        list.add(h1);

        Map<Human, Integer> correctResult = new HashMap<>();
        correctResult.put(h1, 3);
        correctResult.put(h2, 2);
        correctResult.put(new Human("Клавдия", "Тидеманова", "Игоревна", 32, "male", LocalDate.of(1988, 10,10)), 1);
        correctResult.put(new Human("Марта", "Дильсова", "Юрьевна", 17, "female", LocalDate.of(2003, 06, 25)), 1);

        Map<Human, Integer> actualResult = hs.findRepetitions(list);

        assertEquals(correctResult, actualResult);
        assertTrue(actualResult.containsValue(3));
        assertEquals(actualResult.get(h2), 2);
    }
}