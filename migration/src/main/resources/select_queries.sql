1
SELECT count(*) FROM employees WHERE salary = 31000;


2 /* Запрос выводит максимальную из возможных зарплат по отделу */
SELECT department_name, max(max_salary) FROM departments d
         JOIN employees e ON (d.department_id = e.department_id)
         JOIN jobs j ON (e.job_id = j.job_id)
GROUP BY department_name;

/* Запрос выводит максимальную из реальных зарплат по отделу */
SELECT department_name, max(salary)
FROM departments d
         JOIN employees e ON (d.department_id = e.department_id)
GROUP BY department_name;



3
SELECT * FROM locations
         JOIN departments d ON (locations.location_id = d.location_id);


4
DELETE FROM locations a
WHERE a.location_id NOT IN (
    SELECT b.location_id FROM (
        SELECT distinct ON (street_address, city) * from locations c) b);

5
SELECT * FROM employees LIMIT(SELECT count(*)/2 FROM employees);

6
SELECT * FROM employees WHERE department_id IS NULL

7 /* Находится в init_schema.sql */

8
CREATE
OR REPLACE FUNCTION getContactInformation (surname VARCHAR) RETURNS TABLE
                                                     (id INTEGER, first_name VARCHAR, last_name VARCHAR, email VARCHAR, phone_number VARCHAR, department VARCHAR, job VARCHAR)
AS
'select employee_id, first_name, last_name, email, phone_number, department_name, job_title
from employees e JOIN departments d on e.department_id = d.department_id JOIN jobs j on e.job_id = j.job_id
 WHERE last_name = surname;'
    language SQL;

SELECT *
FROM getContactInformation('Klyuev');

9  CREATE
OR REPLACE FUNCTION increaseSalary (department_title VARCHAR, percentage_to_add DECIMAL) returns void
AS
'UPDATE employees SET salary = salary + salary * percentage_to_add WHERE department_id =
(SELECT department_id FROM departments WHERE department_name = department_title)'
    language SQL;

SELECT *
FROM increaseSalary('Front desk', 0.1);


10 CREATE OR REPLACE FUNCTION checkSalary() returns trigger
AS $$begin
        if new.salary < 0 THEN
            RAISE EXCEPTION '%% cannot have a negative salary', NEW.first_name, NEW.last_name;
end if;

end;$$
language plpgsql;

create trigger check_salary_trigger
    AFTER INSERT OR UPDATE ON employees FOR EACH ROW
                        EXECUTE FUNCTION checkSalary();

11
SELECT department_name
FROM departments d
         JOIN employees e ON (d.department_id = e.department_id)
WHERE salary BETWEEN 50000 AND 100000
GROUP BY department_name



