package com.davletova.project.comparator;



import com.davletova.project.model.Human;

import java.util.Comparator;


public class HumanAgeComparator implements Comparator<Human> {

    @Override
    public int compare(Human h1, Human h2) {
        if (h1.getDateOfBirth().isBefore( h2.getDateOfBirth())) {
            return 1;
        } else if (h1.getDateOfBirth().isAfter( h2.getDateOfBirth())) {
            return -1;
        }
        return 0;
    }
}
