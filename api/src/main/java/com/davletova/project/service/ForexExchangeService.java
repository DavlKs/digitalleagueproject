package com.davletova.project.service;

public interface ForexExchangeService {

    Double exchangeMoney(String to, String from, double value);
}
