package com.davletova.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ForexExchangeServiceImpl implements ForexExchangeService {

    @Autowired
    private ForexService forexService;

    @Override
    public Double exchangeMoney(String to, String from, double value) {
        Double rate = forexService.getCurrencyRate(to, from);
        return rate * value;
    }
}
